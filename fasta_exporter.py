#!/usr/bin/env python

import notato
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='+', help='Files to read in')

	args = parser.parse_args()


	for fn in args.infile:
		with open(fn) as f:
			objlist = notato.load(f)
			for iseq in objlist: 
				print(iseq.to_fasta())
