import notato
import argparse


def main(infn, outfn):
	with open(infn) as f:
		seqlist = notato.IndexedMSA.from_clustal(f.read())

		with open(outfn, 'w') as g: g.write(seqlist.dump_json())

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout')

	args = parser.parse_args()

	main(args.infile, args.outfile)
