#!/usr/bin/env python
import notato
import json
import argparse

def load_something(fn):
	with open(fn) as f:
		for l in f:
			if l.strip().startswith('>'):
				f.seek(0)
				return notato.IndexedSequence.from_fasta(f)
			elif l.strip().startswith('CLUSTAL'):
				f.seek(0)
				return notato.IndexedMSA.from_clustal(f)
			else:
				f.seek(0)
				return notato.IndexedSequence.load_json(f)

def main(querylist, subjectlist, outfile, gapopen=-10, gapextend=-2, matrix='EBLOSUM62', mode='glocal', outfmt='json'):
	seqlist = []
	for qseq in querylist:
		for sseq in subjectlist:
			alnlist = qseq.align(sseq, gapopen=gapopen, gapextend=gapextend, matrix=matrix, mode=mode)

			seqlist.extend(alnlist)
	with open(outfile, 'w') as f:
		if outfmt == 'json': 
			f.write(notato.dump_seqlist(seqlist))
		elif outfmt == 'fasta': 
			for seq in seqlist: f.write('{}\n'.format(seq.to_fasta()))
		elif outfmt == 'ranges':
			for seq in seqlist: f.write('>{} {}\n'.format(seq.name, seq.to_rangestr()))
		elif outfmt == 'indices':
			for seq in seqlist: f.write('>{} {}\n'.format(seq.name, seq.to_indices()))
		else: raise ValueError('Unrecognized outfmt "{}"'.format(outfmt))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('query', help='First input filename')
	parser.add_argument('subject', help='Second input filename')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout', help='Where to write the results')

	parser.add_argument('-f', '--gapopen', type=float, default=-10, help='Gap-open penalty (default: -10)')
	parser.add_argument('-g', '--gapextend', type=float, default=-2, help='Gap-extension penalty (default: -2)')
	parser.add_argument('-s', '--matrix', default='EBLOSUM50', help='Scoring matrix (default: EBLOSUM50)')
	parser.add_argument('--mode', default='glocal', help='Alignment mode (default: glocal)')
	parser.add_argument('--outfmt', default='json', help='Output format (\033[1mjson\033[0m, fasta)')

	args = parser.parse_args()

	query = load_something(args.query)
	subject = load_something(args.subject)

	if args.outfmt not in ('json', 'fasta', 'ranges', 'indices'): notato.error('Unknown format "{}"'.format(outfmt))

	main(query, subject, args.outfile, gapopen=args.gapopen, gapextend=args.gapextend, matrix=args.matrix, mode=args.mode, outfmt=args.outfmt)

