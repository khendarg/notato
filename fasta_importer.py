#!/usr/bin/env python
import notato
import argparse


def main(infn, outfn):
	with open(infn) as f:
		seqlist = notato.IndexedSequence.from_fasta(f.read())

		writeme = []
		for seq in seqlist:
			writeme.append(notato.json.loads(seq.dump_json()))
		with open(outfn, 'w') as g: g.write(notato.json.dumps(writeme))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile', nargs='?', default='/dev/stdin')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout')

	args = parser.parse_args()

	main(args.infile, args.outfile)
