#!/usr/bin/env python

#from Bio import motifs
import argparse
import notato
import xml.etree.ElementTree as ET
import os

def get_sequences(f, seqdb=None):
	doc = ET.parse(f)
	root = doc.getroot()
	dbinfo = []
	motiflengths = []

	seqlist = []
	seqdict = {}

	results = []
	for section in root:
		if section.tag == 'motifs':
			for motif in section:
				motiflengths.append(int(motif.attrib['length']))
		elif section.tag == 'sequence_dbs':
			for db in section:
				dbinfo.append(db.attrib['source'])
				if os.path.isfile(db.attrib['source']): g = open(db.attrib['source'])
				elif os.path.isfile(seqdb): g = open(seqdb)
				else: notato.error('Could not find sequence file "{}"'.format(db.attrib['source']))

				seqlist = notato.IndexedSequence.from_fasta(g)
				for seq in seqlist: seqdict[seq.name] = seq
		elif section.tag == 'sequences':
			for sequence in section:
				seqname = sequence.attrib['name']
				seq = seqdict[seqname]
				results.append(seq)
				for seg in sequence:
					if seg.tag != 'seg': continue
					hitid = 0
					for hit in seg:
						if hit.tag == 'hit':
							hitid += 1
							start = int(hit.attrib['pos'])
							motifid = int(hit.attrib['idx'])
							end = int(start - 1 + motiflengths[motifid])
							newdict = {}
							for i in range(start, end+1): newdict[i] = seq[i]
							newname = '{}|MOTIF-{}|HIT-{}'.format(seqname, motifid+1, hitid)

							obj = notato.IndexedSequence(newdict, name=newname)
							obj.start = seq.start
							obj.end = seq.end
							results.append(obj)
					
						
				pass#print(sequence)
	return results

def main(infn, outfn, outfmt='json', seqdb=None):
	with open(infn) as f:
		seqlist = get_sequences(f, seqdb=seqdb)
		#print(seqlist)

		with open(outfn, 'w') as g:
			if outfmt == 'fasta':
				for seq in seqlist: g.write('{}\n'.format(seq.to_fasta()))
			elif outfmt == 'json':
				g.write(notato.dump_seqlist(seqlist))
			elif outfmt == 'ranges':
				for seq in seqlist: g.write('>{} {}\n'.format(seq.name, seq.to_rangestr()))
			elif outfmt == 'indices':
				for seq in seqlist: g.write('>{} {}\n'.format(seq.name, seq.to_indices()))
			else: raise ValueError('Unrecognized outfmt "{}"'.format(outfmt))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', help='Mast results file (mast.txt)')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout', help='Where to write results (default: stdout)')
	parser.add_argument('--outfmt', default='json', help='Output format (\033[1mjson\033[1m, fasta, ranges, indices)')
	parser.add_argument('--seqpath', help='Path to sequence file if it doesn\'t match the one given in mast.xml (most likely by switching to a different directory)')

	args = parser.parse_args()

	if args.outfmt not in ('json', 'fasta', 'ranges', 'indices'): raise notato.error('Unrecognized mode "{}"'.format(args.outfmt))

	main(args.infile, args.outfile, outfmt=args.outfmt, seqdb=args.seqpath)
