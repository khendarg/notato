# notato

## Summary

This set of scripts is intended for exchanging sequence and structure annotations for relatively close homologs.

## Scripts

`*_importer.py`: IO tool for converting sequences, structures, and multiple alignments into Notato IndexedSequence JSON files.

`notato.py`: The module doing the actual work.

`transfer_*.py`: Annotation transfer scripts.

## Dependencies

1. NCBI BLAST+

2. Biopython

3. EMBOSS

4. ClustalW (optional)

## How to use

### Transferring annotations

1. Use an appropriate importer tool to extract annotations in Notato IndexedSequence JSON format:
```
pymol_importer.py SESSIONFILE.pse QUERYFILE.json
mast_importer.py SOMEWHERE/mast.xml QUERYFILE.json
fasta_importer.py SOMETHING.faa QUERYFILE.json
```

2. Transfer annotations as needed:
```
transfer_pairwise.py QUERYFILE.json TARGETSEQ.faa -o OUTPUT.json
transfer_pairwise.py QUERYFILE.json TARGETALN.clu -o OUTPUT.faa --outfmt fasta
transfer_pairwise.py QUERYFILE.json 1PDB 2TRA 3NSF 4ER2 -o OUTPUT.pse --outfmt pse
```

3. Celebrate not having to meddle with indices for files of different formats manually.
