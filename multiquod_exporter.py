#!/usr/bin/env python
from __future__ import print_function, division

import argparse
import json
import notato
import matplotlib.cm

try: input = raw_input
except NameError: pass

def main(infile, outfile):
	pass

def parse2ranges(f):
	iseqlist = notato.IndexedSequence.load_json(f)
	rangedict = {}
	for iseq in iseqlist:
		rangedict[iseq.name] = iseq.to_ranges()

	return rangedict

def get_color(n):
	color = matplotlib.cm.tab10((n % 10) / 10)

	rgb = '#' + ''.join(['{:02x}'.format(int(x*255)) for x in color[:3]])

	return rgb

def prompt_subfeature(ranges, name='', default='domains', color='blue', subplot='SUBPLOTID', y=-3, dy=0.2, noprompt=False):

	if noprompt:
		form = default
		featcolor = color
	else:
		print('Form? (default: {})'.format(default))
		form = input()
		form = default if not form.strip() else form
		print('Using form "{}"'.format(form))

		print('Color? (default: {})'.format(color))
		featcolor = input()
		featcolor = color if not featcolor.strip() else featcolor

	out = '# Plot features for "{}"\n'.format(name)
	if form.startswith('dom'):
		for rng in ranges:
			#TODO: Use shlex.quote to quote strings (if available)
			out += 'add domain {} {} {} {} {} {} "{}"\n'.format(subplot, rng[0], rng[1], y, y+dy, featcolor, name)
		
	elif form.startswith('wall'):
		for rng in ranges:
			out += 'add walls {} {} {} {} {}\n'.format(subplot, rng[0], rng[1], y, dy)

	elif form.startswith('tm'):
		rangestr = ' '.join([' '.join([str(rng[0]), str(rng[1])]) for rng in ranges])

		out += 'tms add {} --color {} {}\n'.format(subplot, featcolor, rangestr)

	else: raise ValueError('Unrecognized subplot feature "{}"'.format(form))

	return out

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--interactive', action='store_true', help='Prompt for type/color')

	parser.add_argument('--form', default='domains', help='Form to use (\033[1mdomains\033[0m, walls, tms)')

	parser.add_argument('--subplot', default='SUBPLOTID', help='Subplot ID (default: SUBPLOTID)')

	parser.add_argument('infile', nargs='+', help='JSON files to read in')
	parser.add_argument('-o', default='/dev/stdout', help='Output filename (default: stdout)')
	parser.add_argument('-f', action='store_true', help='Overwrite outfile')
	parser.add_argument('-y', default=-2.9, type=float, help='Baseline for domains or position for walls')
	parser.add_argument('--dy', default=0.3, type=float, help='Baseline for domains or position for walls')

	args = parser.parse_args()

	rangedict = {}
	for infn in args.infile:
		with open(infn) as f:
			rangedict.update(parse2ranges(f))

	i = 0
	out = ''
	current_y = args.y

	if args.interactive: noprompt = False
	else: noprompt = True

	for k in rangedict:
		out += prompt_subfeature(rangedict[k], name=k, default=args.form, color=get_color(i), subplot=args.subplot, y=current_y, dy=args.dy, noprompt=noprompt)

		i += 1
		if args.form == 'domains': current_y = args.y + args.dy * i
		out += '\n'


	if args.f: 
		with open(args.o, 'w') as f: f.write(out)
	else: 
		with open(args.o, 'a') as f: f.write(out)

	#main(args.infile, args.o, clobber=args.f, interactive
