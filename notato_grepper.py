#!/usr/bin/env python

import notato
import re
import argparse

def main(pattern, fh, invert=False, single=False):
	keeplist = []
	for iseq in notato.IndexedSequence.load_json(fh):
		if bool(re.search(pattern, iseq.name)) ^ invert:
			keeplist.append(iseq)
			if single: return keeplist

	return keeplist

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-v', action='store_true', help='Inverse match')
	parser.add_argument('-c', action='store_true', help='Count matches')
	parser.add_argument('--single', action='store_true', help='Stop at first match')
	parser.add_argument('--outfmt', help='Output format (\033[1mjson\033[0m, fasta, ranges)')

	parser.add_argument('pattern', help='Pattern to grep for')
	parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='Files to read in (default: stdin)')

	args = parser.parse_args()

	results = []
	counts = {}
	for fn in args.infile:
		with open(fn) as f: perfile = main(args.pattern, f, invert=args.v, single=args.single)
		counts[fn] = len(perfile)
		results.extend(perfile)

		if args.single and len(results): break

	if args.c: 
		for fn in counts: print('{}:{}'.format(fn, counts[fn]))
	else:
		if args.outfmt == 'fasta': 
			for iseq in results:
				print(iseq.to_fasta())

		elif args.outfmt == 'ranges':
			for iseq in results:
				print(iseq.to_rangestr())

		else:
			print(notato.dump_seqlist(results))
