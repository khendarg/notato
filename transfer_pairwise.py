#!/usr/bin/env python
from __future__ import print_function, division
import notato
import argparse

def load_json(fn):
	with open(fn) as f:
		return notato.IndexedSequence.load_json(f)

def get_longest(seqlist):
	maxlen = None
	longest = None
	for seq in seqlist:
		if maxlen is None: 
			maxlen = len(seq.seqdict)
			longest = seq
		elif len(seq.seqdict) > maxlen: 
			maxlen = len(seq.seqdict)
			longest = seq
	return longest

def load_something(fn):
	with open(fn) as f:
		for l in f:
			if l.strip().startswith('>'):
				f.seek(0)
				return notato.IndexedSequence.from_fasta(f)
			elif l.strip().startswith('CLUSTAL'):
				f.seek(0)
				return notato.IndexedMSA.from_clustal(f)
			else:
				f.seek(0)
				return notato.IndexedSequence.load_json(f)

def get_by_name(seqlist, name):
	for seq in seqlist:
		if seq.name == name: return seq
	raise IndexError('No sequence named {}'.format(name))
				
def main(query, querylist, target, targetlist, gapopen=-10, gapextend=-2, matrix='EBLOSUM62', outfmt='json', outfile='/dev/stdout'):
	qrmap = query.get_flat_indices(reverse=True)
	qmap = query.get_flat_indices(reverse=False)
	hits = []
	for t in targetlist:
		trmap = t.get_flat_indices(reverse=True)
		#for q in querylist:
		#	if q is query: continue
		aln = notato.IndexedPairwise(*query.align(t))
		atrmap = aln.seq2.get_flat_indices(reverse=True, ignoregaps=False)

		for q in querylist:
			if q is query: continue

			#bridge fullquery to gapless q
			#query2q = query.map_to_identical(aln.seq1)
			query2gaplessq = query.get_flat_indices()


			#bridge gapless q to gapped q
			gaplessq2gappedq = aln.seq1.get_flat_indices(reverse=True)
			#keys = sorted(gaplessq2gappedq.indexdict)
			#print([keys[i] - keys[i-1] for i in range(1, len(keys))])
			
			#bridge gapped q to gapped t (ERROR-LIMITING STEP)
			gappedq2gappedt = aln.seq1.map_to_aligned(aln.seq2)
			#print(aln.seq1.to_fasta())
			#print(aln.seq2.to_fasta())
			#print(gappedq2gappedt.indexdict)

			#bridge gapped t to gapless t
			gappedt2gaplesst = aln.seq2.get_flat_indices()
			#print(aln.seq2.to_fasta())
			#print(gappedt2gaplesst)

			#bridge gapless t to full target
			gaplesst = notato.IndexedSequence.from_fasta(aln.seq2.to_fasta(nogaps=True))[0]
			gaplesst2target = gaplesst.map_to_identical(t)
			#print(aln.seq2.to_fasta(nogaps=True))
			#print(gaplesst.to_fasta())
			#print(gaplesst2target)

			
			resilist = []
			#print(q.name)
			#print(query.to_fasta())
			#print(aln.seq1.to_fasta())
			#print(aln.seq2.to_fasta())
			gaplessq = notato.IndexedSequence.from_fasta(aln.seq1.to_fasta(nogaps=True))[0]
			gaplesst = notato.IndexedSequence.from_fasta(aln.seq2.to_fasta(nogaps=True))[0]
			for resi in sorted(q.seqdict):
				#print('1.', resi, query.seqdict[resi])
				#print('2.', query2gaplessq[resi], gaplessq.seqdict[query2gaplessq[resi]])
				qpos = gaplessq2gappedq[query2gaplessq[resi]]
				#print('3.', qpos, aln.seq1.seqdict[qpos])
				#tpos = gappedq2gappedt[qpos]
				#print('4.', qpos, aln.seq2.seqdict[qpos] if qpos in gappedt2gaplesst else None)
				if qpos in gappedt2gaplesst: resilist.append(gaplesst2target[gappedt2gaplesst[qpos]])
				else: continue
				#print('5.', gappedt2gaplesst[qpos], gaplesst.seqdict[gappedt2gaplesst[qpos]])
				#print('6.', gaplesst2target[gappedt2gaplesst[qpos]], t.seqdict[gaplesst2target[gappedt2gaplesst[qpos]]])

				#if tpos is None: continue
				#elif tpos in gappedt2gaplesst:
				#	resilist.append(gaplesst2target[gappedt2gaplesst[tpos]])
				#else: print('Could not find {} in {}'.format(tpos, t.name))

			newdict = {}
			for resi in resilist: 
				newdict[resi] = t.seqdict[resi]
			name = '{}|{}'.format(t.name, q.name)
			obj = notato.IndexedSequence(newdict, name=name)
			obj.start = t.start
			obj.end = t.end
			hits.append(obj)

	with open(outfile, 'w') as f:
		if outfmt == 'json':
			f.write(notato.dump_seqlist(hits))
		elif outfmt == 'fasta':
			for hit in hits: f.write('{}\n'.format(hit.to_fasta()))
		elif outfmt == 'ranges':
			for hit in hits: f.write('>{} {}\n'.format(hit.name, hit.to_rangestr()))
		elif outfmt == 'indices':
			for hit in hits: f.write('>{} {}\n'.format(hit.name, hit.to_indices()))
		else: raise ValueError('Unrecognized outfmt "{}"'.format(outfmt))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('query', help='Query JSON containing IndexedSequence definitions for fragments of interest and the full sequence')

	parser.add_argument('--qfull', help='Sequence name (default) or IndexedSequence JSON (use "file:FILENAME") containing the full sequence. The longest sequence in the query JSON is selected as the full sequence by default.')

	parser.add_argument('target', help='Target JSON or FASTA to map fragments of residue on')

	parser.add_argument('--singletarget', action='store_true', help='Map fragment(s) onto one single target sequence (likely the longest) instead of all of them')

	parser.add_argument('--tfull', help='Sequence name (default) or IndexedSequence JSON (use "file:FILENAME") containing the full/actual target sequence. All of the sequences in the target JSON are selected as the full sequence by default.')


	parser.add_argument('-f', '--gapopen', default=-10, type=float, help='Gap open penalty')
	parser.add_argument('-g', '--gapextend', default=-10, type=float, help='Gap open penalty')
	parser.add_argument('-s', '--matrix', default='EBLOSUM62', help='Substitution matrix')
	parser.add_argument('-o', '--outfile', default='/dev/stdout', help='Where to write the results')
	parser.add_argument('--outfmt', default='json', help='Output format (\033[1mjson\033[0m, fasta)')

	args = parser.parse_args()

	if args.outfmt not in ('json', 'fasta', 'ranges', 'indices'): raise notato.error('Unrecognized mode "{}"'.format(args.outfmt))

	if not args.query.endswith('json'): raise ValueError('Query should be an IndexedSequence JSON file')
	querylist = load_json(args.query)

	if args.qfull is None: qfull = get_longest(querylist)
	elif args.qfull.startswith('file:'): qfull = load_something(args.qfull[5:])
	else: qfull = get_by_name(querylist, args.qfull)

	targetlist = load_something(args.target)

	if args.singletarget: 
		if args.tfull: 
			if args.tfull.startswith('file:'): tfull = load_something(args.tfull[5:])
			else: tfull = get_by_name(targetlist, args.tfull)
			targetlist = tfull
		else: tfull = get_longest(targetlist)
	else: tfull = None

	qfull.get_flat_indices()

	main(qfull, querylist, tfull, targetlist, gapopen=args.gapopen, gapextend=args.gapextend, matrix=args.matrix, outfmt=args.outfmt, outfile=args.outfile)
