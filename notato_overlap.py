#!/usr/bin/env python
import argparse
import notato

def main(queryfile, subjectfile, min_overlap=1, match_prefixes=True, min_contig=1):
	qiseqlist = notato.IndexedSequence.load_json(queryfile)
	siseqlist = notato.IndexedSequence.load_json(subjectfile)

	results = {}

	for qiseq in qiseqlist:
		results[qiseq.name] = []
		qindices = set(qiseq.seqdict.keys())
		for siseq in siseqlist:
			if match_prefixes and (qiseq.name.split('|')[0] != siseq.name.split('|')[0]): continue

			sindices = set(siseq.seqdict.keys())

			intersection = qindices.intersection(sindices)
			if len(intersection) < min_overlap: continue

			size = 0
			newdict = {}
			for k in sorted(intersection):
				if qiseq.seqdict[k] == siseq.seqdict[k]: 
					size += 1
					newdict[k] = siseq.seqdict[k]

			if size < min_overlap: continue

			newname = '{}|INTERSECTION|{}'.format(qiseq.name, siseq.name)

			newend = min(qiseq.end, siseq.end)

			obj = notato.IndexedSequence(newdict, newname)
			obj.end = newend

			contig = max([rng[1] - rng[0] + 1 for rng in obj.to_ranges()])

			if contig < min_contig: continue

			results[qiseq.name].append((size, obj))

	for k in results:
		for overlap, iseq in sorted(results[k])[::-1]:
			print(iseq.to_fasta())
	pass

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('query')
	parser.add_argument('subject')

	parser.add_argument('--min-overlap', type=int, default=1, help='Minimum overlap between query sequences and subject sequences')
	parser.add_argument('--min-contig', type=int, default=1, help='Minimum contiguous overlap between query sequences and subject sequences')

	parser.add_argument('--match-prefixes', action='store_true', help='Require that query sequences and subject sequences have identical headers up to the first pipe')

	args = parser.parse_args()

	with open(args.query) as qf:
		with open(args.subject) as sf:
			main(qf, sf, min_overlap=args.min_overlap, match_prefixes=args.match_prefixes, min_contig=args.min_contig)
