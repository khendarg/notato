#!/usr/bin/env python2
from __future__ import print_function, division

import notato
import pymol
import argparse
import sys
import re
import json

import time

import Bio.PDB
try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

def debug(*things): print('[DEBUG]', *things, file=sys.stderr)
def info(*things): print('[INFO]', *things, file=sys.stderr)
def warn(*things): print('[WARNING]', *things, file=sys.stderr)
def error(*things): 
	print('[ERROR]', *things, file=sys.stderr)
	exit(1)

START = time.time()

def extend_seqdict(seqdict, resi, resn):
	if resn in CODE: seqdict[int(resi)] = CODE[resn]

def main(infn, outfn, ignoresele=True, ignoreprivate=True, outfmt='json'):

	sys.argv = sys.argv[:1]
	sys.argv.extend(['-cq'])

	pymol.finish_launching()

	pymol.cmd.load(infn)
	pymol.finish_launching()

	selections = [name for name in pymol.cmd.get_names('selections') if name != 'sele']

	if not selections: error('Please define at least one selection') #maybe dump everything to sequence instead?


	fullseqdict = {}
	finalresi = {}

	#writeme = []
	results = []

	for selename in selections:

		if ignoresele and (selename == 'sele'): 
			info('Ignored selection "sele"')
			continue
		if ignoreprivate and selename.startswith('_'):
			info('Ignored selection "{}"'.format(selename))
			continue
		info('Found selection "{}"'.format(selename))
		
		relchains = set()
		seqdict = {}
		namespace = {'info':info, 'relchains':relchains, 'seqdict':seqdict, 'int':int, 'CODE':CODE, 'extend_seqdict':extend_seqdict}
		resimapdict = {}
		
		#pymol.cmd.iterate(selename, 'info(chain,resi,resn,name)', space=namespace)
		pymol.cmd.iterate(selename, 'relchains.add(chain)', space=namespace)
		for chain in relchains: 
			seqdict[chain] = {}
			resimapdict[chain] = {}
		pymol.cmd.iterate(selename, 'extend_seqdict(seqdict[chain], resi, resn)', space=namespace)

		for chain in relchains: 
			if chain in fullseqdict: fullseq = fullseqdict[chain]
			else: 
				fsd = {}
				namespace['fsd'] = fsd
				pymol.cmd.iterate('c. {}'.format(chain), 'extend_seqdict(fsd, resi, resn)', space=namespace)
				fullseq = notato.IndexedSequence.from_dict(fsd)
				fullseq.name = 'CHAIN_{}'.format(chain)
				fullseq.start = min(fullseq.seqdict)
				if fullseq.start < 0: notato.warn('Starting index < 0 for chain {}'.format(chain))
				fullseq.end = max(fullseq.seqdict)
				fullseqdict[chain] = fullseq

				#blastresults = fullseq.blastp(db='uniprot_sprot', evalue='1e-5', max_target_seqs='20')
				#resimap = fullseq.blastp_and_map(db='uniprot_sprot', evalue='1e-5', max_target_seqs='20')
				#for acc in resimap:
				#	resimapdict[chain][acc] = resimap[acc]
				results.append(fullseq)
				#if outfmt == 'json': writeme.append(notato.json.loads(fullseq.dump_json()))
				#elif outfmt == 'fasta': writeme.append(fullseq.to_fasta())

			inseq = notato.IndexedSequence.from_dict(seqdict[chain])
			inseq.name = selename
			inseq.start = fullseqdict[chain].start
			inseq.end = fullseqdict[chain].end
			results.append(inseq)
			#if outfmt == 'json': writeme.append(json.loads(inseq.dump_json()))
			#elif outfmt == 'fasta': writeme.append(inseq.to_fasta())

	with open(outfn, 'w') as f:
		if outfmt == 'json': 
			f.write(notato.dump_seqlist(results))
		elif outfmt == 'fasta': 
			for seq in results: f.write('{}\n'.format(seq.to_fasta()))
		elif outfmt == 'ranges':
			for seq in results: f.write('>{} {}\n'.format(seq.name, seq.to_rangestr()))
		elif outfmt == 'indices':
			for seq in results: f.write('>{} {}\n'.format(seq.name, seq.to_indices()))
		else: raise ValueError('Unrecognized outfmt "{}"'.format(outfmt))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile')
	parser.add_argument('--allow-sele', action='store_true', help='Don\'t ignore the selection (sele)')
	parser.add_argument('--allow-private', action='store_true', help='Don\'t ignore private selections, i.e. those starting with "_"')
	parser.add_argument('--outfmt', default='json', help='Output format (\033[1mjson\033[0m, fasta, ranges, indices)')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout')
	args = parser.parse_args()

	if args.outfmt not in ('json', 'fasta', 'ranges', 'indices'): notato.error('Unrecognized mode "{}"'.format(args.outfmt))

	main(args.infile, args.outfile, ignoresele=not args.allow_sele, ignoreprivate=not args.allow_private, outfmt=args.outfmt)
