#!/usr/bin/env python

import json
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='*', default=['/dev/stdin'])

	args = parser.parse_args()

	outlist = []

	for fn in args.infile:
		with open(fn) as f:
			outlist.extend(json.load(f))

	print(json.dumps(outlist))
