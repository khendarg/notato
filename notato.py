from __future__ import division, print_function
import Bio.Blast.NCBIXML as NCBIXML
import io
import subprocess
import re
import sys
import json
import tempfile
from Bio import SeqIO, AlignIO

def debug(*things): print('[DEBUG]', *things, file=sys.stderr)
def info(*things): print('[INFO]', *things, file=sys.stderr)
def warn(*things): print('[WARNING]', *things, file=sys.stderr)
def error(*things): 
	print('[ERROR]', *things, file=sys.stderr)
	exit(1)

class IndexedSequence(object):
	''' Class for handling sequences where the indices actually matter '''
	name = None
	seq = None
	seqdict = None

	start = None
	end = None

	annotations = None

	def __init__(self, seqdict=None, name='untitled'):
		''' Constructor

		seqdict : dict-like, optional
			Initialize this object with a specific {resi:resn} dictionary
		name : str, optional
			Specify a name for the sequence
		'''
		self.seqdict = {} if seqdict is None else seqdict
		self.name = name
		self.annotations = []

	@staticmethod
	def load_json(thing):
		''' Factory method for loading an IndexedSequence JSON

		thing : file or str
			Whence to grab the data
		'''
		if isinstance(thing, str):
			obj = json.loads(thing)
		else:
			obj = json.load(thing)
		if type(obj) is dict: objlist = [obj]
		elif type(obj) is list: objlist = obj
		else: raise ValueError('Invalid JSON (not an object')

		newobjlist = []
		for obj in objlist:
			if obj['type'] != "IndexedSequence": raise ValueError('Invalid JSON (obj.type != "IndexedSequence"')
			newobj = IndexedSequence()
			seqdict = obj.get('seqdict', {})
			newobj.seqdict = {}
			for k in seqdict: newobj.seqdict[int(k)] = seqdict[k]

			newobj.name = obj.get('name', 'untitled')

			if 'start' in obj: newobj.start = obj['start']
			elif obj['seqdict']: newobj.start = min(newobj.seqdict)
			else: newobj.start = None

			if 'end' in obj: newobj.end = obj['end']
			elif obj['seqdict']: newobj.end = max(newobj.seqdict)
			else: newobj.end = None

			newobjlist.append(newobj)
		return newobjlist

	def dump_dict(self):
		obj = {}
		obj['type'] = 'IndexedSequence'
		obj['start'] = self.start
		obj['end'] = self.end
		obj['seqdict'] = self.seqdict
		obj['name'] = self.name

		return obj

	def dump_json(self):
		''' Dumps the contents of this object to JSON '''

		return json.dumps(self.dump_dict())

	@staticmethod
	def from_dict(somedict):
		''' Deprecated. Alternate constructor accepting dict '''
		newseq = IndexedSequence()

		for resi in somedict:
			if type(resi) is not int: raise ValueError('All keys in `somedict\' must be ints')
			resn = somedict[resi]
			if type(resn) is not str: raise ValueError('All values in `somedict\' must be strs')
			if len(resn) != 1: raise ValueError('All values in `somedict\' must be one-letter codes')

			newseq.seqdict[resi] = resn

		return newseq

	@staticmethod
	def from_blastdb(accession, **kwargs):
		''' Retrieves IndexedSequence objects given accessions and a BLAST database

		accession : str, required
			Accession to pull from database

		db : str, required
			BLAST database to consult

		Other kwargs are pulled and passed to BLAST
		'''
		dbcmdargs = []
		for kw in kwargs: dbcmdargs.extend(['-' + kw, kwargs[kw]])
		dbcmdargs.extend('-entry', accession)

		rawout = subprocess.check_output(['blastdbcmd'] + dbcmdargs)

		return IndexedSequence.from_fasta(rawout)

	@staticmethod
	def from_fasta(fastastr):
		''' Alternate constructor from a FASTA string or file

		fastastr : str or file
			What to load
		'''
		if isinstance(fastastr, str): stream = io.BytesIO(fastastr.encode('utf-8'))
		else: stream = fastastr

		objlist = []

		for record in SeqIO.parse(stream, 'fasta'):
			obj = IndexedSequence()
			obj.name = record.name
			seqdict = {}

			startgap = re.search('^(-+)', str(record.seq))
			#+1 for 1-indexing
			if startgap: startoffset = (startgap.end() - startgap.start()) + 1 
			else: startoffset = 1
			obj.start = 1

			#endgap = re.search('(-+)$', str(record.seq))
			#+1 for interval
			#if endgap: obj.end = (endgap.end() - startgap.end()) + 1
			#else: obj.end = 0
			obj.end = 0

			trueend = 0
			for i, c in enumerate(record.seq):

				obj.end += 1

				if c == '-': continue
				else: 
					seqdict[i+startoffset] = c
					trueend = i+1

			obj.seqdict = seqdict
			#obj.start = 1
			#obj.end = i+1

			objlist.append(obj)
		return objlist

	@staticmethod
	def from_seq(seq, start=1, name='untitled', ignoregaps=False):
		''' Alternate constructor for raw sequences

		seq : str
			Sequence to parse
		start : int, optional
			Default starting residue
		name : str, optional
			What to name the resulting sequence
		ignoregaps : bool, optional
			Collapse gaps if set
		'''
		i = start
		pattern = '[A-Za-z'
		if not ignoregaps: pattern += '\-'
		pattern += ']'
		seqdict = {}

		for c in seq:
			if not re.match(pattern, c): continue
			seqdict[i] = c
			i += 1
		obj = IndexedSequence(seqdict=seqdict, name=name)
		obj.start = start
		obj.end = max(i-1, 1)
		return obj

	def to_fasta(self, nogaps=False):
		''' Writes to FASTA format

		nogaps : bool, optional
			Collapse gaps in output
		'''
		name = self.name if self.name else 'untitled'
		if nogaps:
			seq = ''
			for resi in sorted(self.seqdict):
				resn = self.seqdict[resi]
				if resn == '-': continue
				else: seq += resn
			#seq = ''.join([self.seqdict[resi] for resi in sorted(self.seqdict)])
		else:
			start = 1 if self.start is None else self.start
			if not self.seqdict:
				if self.end is not None and self.start is not None:
					seq = '-' * (self.end - self.start)
				else: seq = ''

			else:
				seq = '-' * max(min(self.seqdict) - start, 0)
				lastresi = None
				for resi in sorted(self.seqdict):
					if lastresi is None: pass
					elif resi > (lastresi + 1): seq += '-' * (resi - lastresi - 1)
					#elif resi == (lastresi + 1): pass
					seq += self.seqdict[resi]
					lastresi = resi

				if self.end: 
					seq += (self.end - max(self.seqdict)) * '-'

		return '>{}\n{}'.format(name, seq)

	def to_ranges(self, ignoregaps=True):
		ranges = []
		last = None
		for resi in sorted(self.seqdict):
			resn = self.seqdict[resi]
			if ignoregaps and resn == '-': continue

			if last is None:
				ranges.append([resi, resi])
			elif resi == last + 1:
				ranges[-1][-1] = resi
			else:
				ranges.append([resi, resi])
			last = resi
		return ranges

	def to_rangestr(self, ignoregaps=True):
		ranges = self.to_ranges(ignoregaps=ignoregaps)
		pieces = []
		for r in ranges:
			if r[0] == r[1]: pieces.append(str(r[0]))
			else: pieces.append('{}:{}'.format(*r))
		return ','.join(pieces)

	def to_indices(self, ignoregaps=True):
		if ignoregaps:
			indices = []
			for resi in sorted(self.seqdict):
				resn = self.seqdict[resi]
				if resn == '-': continue
				indices.append(resi)
		else: indices = sorted(self.seqdict)

		return ','.join([str(x) for x in sorted(indices)])

	def align(self, other, **kwargs):
		mode = kwargs.get('mode', 'glocal')
		matrix = kwargs.get('matrix', 'EBLOSUM62')
		gapextend = kwargs.get('gapextend', -2)
		gapopen = kwargs.get('gapopen', -10)

		f1 = tempfile.NamedTemporaryFile()
		f1.write(self.to_fasta())
		f1.flush()

		f2 = tempfile.NamedTemporaryFile()
		f2.write(other.to_fasta())
		f2.flush()

		if mode.startswith('glo'): program = 'needle'
		else: program = 'water'

		cmd = [program, '-asequence', f1.name, '-bsequence', f2.name, '-outfile', '/dev/stdout', '-gapopen', str(-gapopen), '-gapextend', str(-gapextend), '-datafile', matrix]

		out = subprocess.check_output(cmd)
		outstream = io.BytesIO(out)
		result = AlignIO.parse(outstream, 'emboss')
		outlist = []
		for record in result: 
			for seq in record: 
				fasta = '>{}\n{}'.format(seq.description, seq.seq)
				outlist.append(IndexedSequence.from_fasta(fasta)[0])
				#debug(seq.id, seq)
		return outlist

	def __getitem__(self, index): 
		if isinstance(index, slice):
			step = index.step if index.step else 1
			return [self.seqdict[i] for i in range(index.start, index.stop, step) if i in self.seqdict]
		elif isinstance(index, tuple):
			result = []
			for thing in index:
				if type(thing) is int: result.append(self[thing])
				else: result.extend(self[thing])
			return result
		else:
			return self.seqdict[index]
			#can raise KeyError (probably for the best, really)


	def __contains__(self, thing): return self.seqdict.__contains__(thing)

	def __iter__(self): pass

	def get_indices(self): pass

	def get_flat_indices(self, reverse=False, ignoregaps=True):
		''' Returns a mapping between the resi-like and the nogaps-like indices 

		reverse : bool, optional
			Return the flat-to-full mapping instead of the full-to-flat one
		'''

		i = 1
		mapping = {}
		for resi in sorted(self.seqdict):
			resn = self.seqdict[resi]
			if ignoregaps:
				if not re.match('[A-Za-z]', resn): continue
			elif not re.match('[A-Za-z]\-', resn): continue

			if reverse: mapping[i] = resi
			else: mapping[resi] = i
			
			i += 1

		return BetterDict(mapping)

	def get_collapsed(self, flatten=False):
		s = self.to_fasta(nogaps=False)

		ngaps = re.findall('\n-+[^A-Z]', s)
		nterm = len(ngaps[0][1:-1]) if ngaps else 0
		cgaps = re.findall('-+$', s)
		cterm = len(cgaps[0]) if cgaps else 0

		return IndexedSequence.from_fasta(self.to_fasta(nogaps=True))[0]

	def map_to(self, other, allowmismatch=False):
		if not allowmismatch and ((self.end - self.start) != (other.end - other.start)): raise IndexError('Unequal lengths between {} and {}'.format(self.name, other.name))
		mapdict = {}
		for resi1, resi2 in zip(sorted(self.seqdict), sorted(other.seqdict)):
			mapdict[resi1] = resi2

		return BetterDict(mapdict)

	def map_to_identical(self, other):
		seq1 = self.to_fasta(nogaps=True)
		seq2 = other.to_fasta(nogaps=True)
		if seq1[seq1.find('\n'):] != seq2[seq2.find('\n'):]: raise IndexError('Non-identical sequences detected')

		map1 = self.get_flat_indices(reverse=True)
		map2 = other.get_flat_indices(reverse=True)
		newmap = {}

		for k in map1: newmap[map1[k]] = map2[k]

		return BetterDict(newmap)

	def map_to_aligned(self, other):
		if (self.end - self.start) != (other.end - other.start): warn('Unequal lengths while mapping to aligned')

		keys1 = sorted(self.seqdict)
		keys2 = sorted(other.seqdict)
		mapdict = {}
		i1 = 1
		i2 = 1
		for resi1, resi2 in zip(keys1, keys2):
			resn1 = self.seqdict[resi1]
			resn2 = other.seqdict[resi2]

			if resn1 == '-' and resn2 == '-': pass
			elif resn2 == '-': mapdict[i1] = None
			elif resn1 == '-': pass
			else: mapdict[i1] = i2

			if resn1 != '-': i1 += 1
			if resn2 != '-': i2 += 1

		return BetterDict(mapdict)


	def map_to_gapless(self, reverse=False):
		i = 1
		newmap = {}
		for resi in sorted(self.seqdict):
			resn = self.seqdict[resi]
			if resn != '-': 
				if reverse: newmap[i] = resi
				else: newmap[resi] = i
				i += 1
		return BetterDict(newmap)

	def get_sequence(self): pass

	def __dict__(self): pass

	def insert_residue(self, residue, pos=None): pass

	def add_annotation(self, annotation): pass

	def get_annotations(self): pass

	def transfer_annotations(self, other): pass

	def blastp(self, **kwargs):
		outfmt = '5'

		blastargs = []
		for kw in kwargs: blastargs.extend(['-' + kw, str(kwargs[kw])])

		cmd = ['blastp', '-outfmt', '5'] + blastargs

		p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

		out, err = p.communicate(input=self.to_fasta().encode('utf-8'))

		xmlout = io.BytesIO(out)
		return list(NCBIXML.parse(xmlout))

	def blastp_and_map(self, count=1, **kwargs):
		table = self.blastp(**kwargs)

		qmap = {}
		for i, resi in enumerate(self.seqdict):
			qmap[i+1] = resi

		maps = {}
		finished = 0
		for record in table:
			for alignment in record.alignments:
				maptosubject = {}
				for hsp in alignment.hsps:
					pident = hsp.identities / hsp.align_length
					if pident < 0.2:
						warn('Very low identity ({:0.0%} < 20%) for {} vs. {}'.format(pident, self.name, alignment.accession))
					elif pident < 0.4:
						warn('Low identity ({:0.0%} < 40%) for {} vs. {}'.format(pident, self.name, alignment.accession))
					#elif pident < 0.8:
					#	warn('Decent identity ({:0.0%} < 60%) for {} vs. {}'.format(pident, self.name, alignment.accession))
					#elif pident < 1.0:
					#	warn('Great identity ({:0.0%} < 100%) for {} vs. {}'.format(pident, self.name, alignment.accession))
					subject = IndexedSequence.from_seq(hsp.sbjct, start=hsp.sbjct_start, ignoregaps=True)
					#print(subject.seqdict)
					qi = hsp.query_start
					si = hsp.sbjct_start
					for qr, sr in zip(hsp.query, hsp.sbjct):
						if re.match('[A-Za-z]', qr) and re.match('[A-Za-z]', sr): 
							maptosubject[qmap[qi]] = si

						if re.match('[A-Za-z]', qr): qi += 1
						if re.match('[A-Za-z]', sr): si += 1
				finished += 1
				maps[alignment.accession] = BetterDict(maptosubject)
				if finished >= count: break
		return maps

class IndexedMSA(IndexedSequence):
	seqlist = None

	@staticmethod
	def from_clustal(thing, fmt='clustal'):
		if isinstance(thing, str): stream = io.BytesIO(thing.encode('utf-8'))
		else: stream = thing

		alignments = AlignIO.parse(stream, 'clustal')
		newobj = IndexedMSA()
		newobj.seqlist = []
		for msa in alignments: 
			for seq in msa:
				fasta = '>{}\n{}'.format(seq.description, seq.seq)
				seqobj = IndexedSequence.from_fasta(fasta)[0]
				newobj.seqlist.append(seqobj)
		return newobj

	def dump_json(self):
		obj = []
		for seq in self.seqlist:
			obj.append(json.loads(seq.dump_json()))
		return json.dumps(obj)

	def __getitem__(self, index): pass #like numpy, [row(s):column(s)]

	def __iter__(self): return iter(self.seqlist)

	def get_columns(self): pass

	def propagate_annotations(self, rowindex): pass #propagate annoations from self[rowindex,:] to other sequences

	def get_annotations(self): pass

	def add_annotation(self, annotation): pass

class Annotation(IndexedSequence):
	seq = None #IndexedSequence object

	name = ''

	pos = None #if necessary, RLE it

	def __init__(self): pass

	def __iter__(self): pass

	def transfer_annotation(self, newseq): pass

class BetterDict(dict):

	def __init__(self, indexdict=None): 
		self.indexdict = {} if indexdict is None else indexdict

	def __getitem__(self, index): 
		if isinstance(index, slice):
			step = index.step if index.step else 1
			#return [i for i in range(index.start, index.stop, step) if i in self.indexdict]
			return [self.indexdict[i] for i in range(index.start, index.stop, step) if i in self.indexdict]
		elif isinstance(index, tuple):
			result = []
			for thing in index:
				if type(thing) is int: result.append(self[thing])
				else: result.extend(self[thing])
			return result
		else:
			return self.indexdict[index]
			#can raise KeyError (probably for the best, really)

	def __contains__(self, thing): return self.indexdict.__contains__(thing)
	def __iter__(self): return iter(self.indexdict)
	def __str__(self): return 'BetterDict({})'.format(self.indexdict)
	def __repr__(self): return 'BetterDict({})'.format(repr(self.indexdict))

class IndexedPairwise(object):
	def __init__(self, seq1, seq2):
		self.seq1 = seq1

		self.seq2 = seq2

	def in_seq(self, index, seqid):
		if seqid not in (1, 2): raise ValueError('Invalid seqid')

		for resi1, resi2 in zip(sorted(self.seq1.seqdict), sorted(self.seq2.seqdict)):
			if resi1 != index: continue
			resn1 = self.seq1.seqdict[resi1]
			resn2 = self.seq2.seqdict[resi2]
			if seqid == 1: 
				if index in self.seq1.seqdict:
					if resn1 != '-' and resn2 != '-': return resi2
			elif seqid == 2: 
				if index in self.seq2.seqdict:
					if resn1 != '-' and resn2 != '-': return resi1
		return None

def dump_seqlist(seqlist):
	obj = []
	for seq in seqlist: obj.append(json.loads(seq.dump_json()))
	return json.dumps(obj)
